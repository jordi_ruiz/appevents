﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EventsDAL.ClassesObjecte;

namespace appEvents
{
    /// <summary>
    /// Interaction logic for ControlEvent.xaml
    /// </summary>
    public partial class ControlEvent : UserControl
    {
        public event RoutedEventHandler PublicarClick;
        public event RoutedEventHandler EditarClick;
        public event RoutedEventHandler EliminarClick;

        public ControlEvent()
        {
            InitializeComponent();
        }

        public string Titol
        {
            get { return txtTitolEvent.Header.ToString(); }
            set { txtTitolEvent.Header = value; }
        }

        public string dataPublicacio
        {
            get { return txtDataPulicacio.Content.ToString(); }
            set { txtDataPulicacio.Content = value; }
        }

        public string dataEvent
        {
            get { return txtDataEvent.Content.ToString(); }
            set { txtDataEvent.Content = value; }
        }

        public string localitatEvent
        {
            get { return txtLocalitat.Content.ToString(); }
            set { txtLocalitat.Content = value; }
        }
        public string descripcioEvent
        {
            get { return txtDescripcio.Text; }
            set { txtDescripcio.Text = value; }
        }

        private void btnPublicar_Click(object sender, RoutedEventArgs e)
        {
            if (PublicarClick != null)
            {
                PublicarClick(this, e);
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            EditarClick(this, e);
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            EliminarClick(this, e);
        }
    }
}
