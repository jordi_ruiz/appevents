﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using EventsDAL;
using EventsDAL.ClassesObjecte;
using System.Data;
using MahApps.Metro.Controls.Dialogs;

namespace appEvents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        private string Accio;

        public MainWindow()
        {
            InitializeComponent();
            cmbPoblacions.ItemsSource = DAL.GetCities();
            cmbPoblacions.SelectedValuePath = "codi";
            cmbPoblacions.DisplayMemberPath = "nom";

            cmbPoblacions2.ItemsSource = DAL.GetCities();
            cmbPoblacions2.SelectedValuePath = "codi";
            cmbPoblacions2.DisplayMemberPath = "nom";
            OmplirComboUsuaris();
            
        }

        private void OmplirComboUsuaris()
        {
            dgUsuaris.ItemsSource = DAL.GetUsers();
            dgUsuaris.AutoGenerateColumns = false;
        }

        private void ObrirEvents_Click(object sender, RoutedEventArgs e)
        {
            frmEvents finestraEvents = new frmEvents();
            finestraEvents.Show();
        }

        private async void btnAcceptar_Click(object sender, RoutedEventArgs e)
        {
            Usuari u = new Usuari(
                txtNif.Text,
                txtNom.Text,
                txtCognoms.Text,
                txtDataNaixement.Text,
                txtCorreuElectronic.Text,
                cmbPoblacions2.SelectedValue.ToString()
                );

            if (Accio == "Add")
            {
                DAL.InsertUser(u);
                OmplirComboUsuaris();
                txtNom.IsReadOnly = true;
                txtCognoms.IsReadOnly = true;
                txtCorreuElectronic.IsReadOnly = true;
                txtDataNaixement.IsEnabled = false;
                cmbPoblacions2.IsEnabled = false;
                txtNif.IsReadOnly = true;
                btnDeleteUser.IsEnabled = true;
                btnAddUser.IsEnabled = true;
                btnUpdateUser.IsEnabled = true;

            }
            else if (Accio == "Delete")
            {
                var result = await this.ShowMessageAsync("Eliminar", "Vols eliminar el client?", MessageDialogStyle.AffirmativeAndNegative);
                if (result == MessageDialogResult.Affirmative)
                {

                }
            }
            else if (Accio == "Update")
            {
                DAL.UpdateUser(u);
                OmplirComboUsuaris();
                btnDeleteUser.IsEnabled = true;
                btnAddUser.IsEnabled = true;
                btnUpdateUser.IsEnabled = true;

            }
        }

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            Accio = "Add";
            txtNom.IsReadOnly = false;
            txtCognoms.IsReadOnly = false;
            txtCorreuElectronic.IsReadOnly = false;
            txtDataNaixement.IsEnabled = true;
            cmbPoblacions2.IsEnabled = true;
            txtNif.IsReadOnly = false;

            txtNom.Clear();
            txtNif.Clear();
            txtCognoms.Clear();
            txtCorreuElectronic.Clear();
            txtDataNaixement.Text = "";
            cmbPoblacions2.SelectedIndex = 0;

            btnUpdateUser.IsEnabled = false;
            Label l = (Label) btnUpdateUser.Content;
            l.Foreground = Brushes.Gray;
            btnDeleteUser.IsEnabled = false;
            l = (Label)btnDeleteUser.Content;
            l.Foreground = Brushes.Gray;
        }

        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            Accio = "Delete";
        }

        private void btnUpdateUser_Click(object sender, RoutedEventArgs e)
        {
            Accio = "Update";
            txtNom.IsReadOnly = false;
            txtCognoms.IsReadOnly = false;
            txtCorreuElectronic.IsReadOnly = false;
            txtDataNaixement.IsEnabled = true;
            cmbPoblacions2.IsEnabled = true;
            txtNif.IsReadOnly = false;

            btnAddUser.IsEnabled = false;
            btnDeleteUser.IsEnabled = false;

        }

        private void ActualitzarDadesUsuari(object sender, SelectedCellsChangedEventArgs e)
        {
            if (e.AddedCells.Count > 0)
            {
                DataRowView fila = (DataRowView)e.AddedCells[0].Item;

                txtNif.Text = fila.Row.ItemArray[0].ToString();
                Usuari u = DAL.GetUserByNif(txtNif.Text);
                txtNom.Text = u.Nom;
                txtCognoms.Text = u.Cognoms;
                txtDataNaixement.Text = u.Data_naixement.ToString();
                txtCorreuElectronic.Text = u.Correu;
                cmbPoblacions2.SelectedValue = u.Poblacio;
            }
        }
    }
}
