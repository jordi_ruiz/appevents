﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace appEvents
{
    /// <summary>
    /// Interaction logic for frmEvents.xaml
    /// </summary>
    public partial class frmEvents : Window
    {
        public frmEvents()
        {
            InitializeComponent();
            ControlEvent ev = new ControlEvent();
            ev.Titol = "Examen de M7";
            ev.descripcioEvent = "L'examen consistirà en crear un projecte....";
            ev.dataEvent = "28.11.2017";
            ev.dataPublicacio = "21.11.2017";
            ev.localitatEvent = "Tàrrega";
            ev.Height = 190;
            ev.Width = 230;

            contenidorEvents.VerticalAlignment = VerticalAlignment.Top;
            contenidorEvents.Children.Clear();
            contenidorEvents.Children.Add(ev);

            for (Int32 i = 0; i < 5; i++)
            {
                ControlEvent ev2 = new ControlEvent();
                ev2.Titol = "Examen de M7";
                ev2.descripcioEvent = "L'examen consistirà en crear un projecte....";
                ev2.dataEvent = "28.11.2017";
                ev2.dataPublicacio = "21.11.2017";
                ev2.localitatEvent = "Tàrrega";
                ev2.Height = 190;
                ev2.Width = 230;
                ev2.Padding = new Thickness(0, 20, 0, 0);
                contenidorEvents.Children.Add(ev2);
            }
           
        }

        


    }
}
