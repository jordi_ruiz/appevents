﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsDAL.ClassesObjecte
{
    public class Evento
    {
        #region Atributs
        public Int32 Id { get; set; }
        public string Titol { get; set; }
        public string Descripcio { get; set; }
        public DateTime DataEvent { get; set; }
        public DateTime DataPublicacio { get; set; }
        public string Ubicacio { get; set; }
        #endregion

        #region Constructors
        public Evento() { }

        public Evento(Int32 _id, string _titol, string _descripcio, DateTime _dataEvent, DateTime _dataPublicacio, string _ubicacio)
        {
            Id = _id;
            Titol = _titol;
            Descripcio = _descripcio;
            DataEvent = _dataEvent;
            DataPublicacio = _dataPublicacio;
            Ubicacio = _ubicacio;
        }

        public Evento(Int32 _id, string _titol, string _descripcio, string _dataEvent, string _dataPublicacio, string _ubicacio)
        {
            Id = _id;
            Titol = _titol;
            Descripcio = _descripcio;
            DataEvent = Convert.ToDateTime(_dataEvent);
            DataPublicacio = Convert.ToDateTime(_dataPublicacio);
            Ubicacio = _ubicacio;
        }

        #endregion
    }
}
