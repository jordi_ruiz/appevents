﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsDAL.ClassesObjecte
{
    public class Usuari
    {
        #region Atributs
        public string Nif { get; set; }
        public string Nom { get; set; }
        public string Cognoms { get; set; }
        public DateTime Data_naixement { get; set; }
        public string Correu { get; set; }
        public string Poblacio { get; set; }
        #endregion

        #region Constructors
        //Constructor buit
        public Usuari() { }

        //Constructor amb tots els atributs
        public Usuari(string _nif, string _nom, string _cognoms, DateTime _dataNaixement, string _correu, string _poblacio)
        {
            this.Nif = _nif;
            this.Nom = _nom;
            this.Cognoms = _cognoms;
            this.Data_naixement = _dataNaixement;
            this.Correu = _correu;
            this.Poblacio = _poblacio;
        }

        //Constructor amb la data en format string
        public Usuari(string _nif, string _nom, string _cognoms, string _dataNaixement, string _correu, string _poblacio)
        {
            this.Nif = _nif;
            this.Nom = _nom;
            this.Cognoms = _cognoms;
            this.Data_naixement = Convert.ToDateTime(_dataNaixement);
            this.Correu = _correu;
            this.Poblacio = _poblacio;
        }
        #endregion

    }

}
