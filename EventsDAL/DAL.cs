using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Data;  // Per treballar amb DataSet, DataTable, DataView....
using System.Collections.ObjectModel;  // Per treballar amb ObservableCollection
using EventsDAL.ClassesObjecte;

namespace EventsDAL
{
    public static class DAL
    {
        
        private static MySqlConnection con;
        
        private static void Connect()
        {
            con = new MySqlConnection("server=10.10.10.10;user=root;pwd=minerva14;database=events");
            con.Open();        
        }

        #region Mètodes per treballar amb els usuaris

            // Utilitzarem el mètode GetUsers per carregar els usuaris a un ComboBox o un Datagrid
            public static DataView GetUsers()
            {
                DataTable dt = new DataTable();
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("SELECT u.nif, u.nom, u.cognoms, CONCAT( u.nom,  \" \", u.cognoms ) AS nom_complert, u.data_naixement, u.correu_electronic, p.nom as nom_poblacio FROM usuaris u, poblacions p WHERE u.codi_poblacio = p.codi ORDER BY u.cognoms ASC, u.nom ASC", con);
                    MySqlDataAdapter da = new MySqlDataAdapter(sql);
                    da.Fill(dt);

                    con.Close();
                }
                catch (Exception e)
                {

                }
                return dt.DefaultView;
            }

            public static Usuari GetUserByNif(string _nif) {
                Usuari u = new Usuari();
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("SELECT nom, cognoms, data_naixement, codi_poblacio, correu_electronic FROM usuaris WHERE nif=@nif", con);
                    sql.Parameters.AddWithValue("@nif", _nif);
                    MySqlDataReader dr = sql.ExecuteReader();

                    if (dr.Read()) {
                        u.Nif = _nif;
                        u.Nom = dr.GetString("nom");
                        u.Cognoms = dr.GetString("cognoms");
                        u.Data_naixement = dr.GetDateTime("data_naixement");
                        u.Poblacio = dr.GetString("codi_poblacio");
                        u.Correu = dr.GetString("correu_electronic");
                    }
                    con.Close();
                }
                catch (Exception e)
                {

                }

                return u;
            }

            //Utilitzarem el mètode InsertUser per inserir un usuari a la base de dades
            public static bool InsertUser(Usuari u)
            {
                bool status = false;
                try { 
                    Connect();
                    MySqlCommand sql = new MySqlCommand("INSERT INTO usuaris (nif, nom, cognoms, data_naixement, correu_electronic, codi_poblacio) VALUES(@nif, @nom, @cognoms,@data,@correu,@codiPob)", con);
                    sql.Parameters.AddWithValue("@nif", u.Nif);
                    sql.Parameters.AddWithValue("@nom", u.Nom);
                    sql.Parameters.AddWithValue("@cognoms", u.Cognoms);
                    sql.Parameters.AddWithValue("@data", u.Data_naixement);
                    sql.Parameters.AddWithValue("@correu", u.Correu);
                    sql.Parameters.AddWithValue("@codiPob", u.Poblacio);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

            //Utilitzarem el mètode DeleteUser per eliminar un usuari de la base de dades
            public static bool DeleteUser(string nif)
            {
                bool status = false;
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("DELETE FROM usuaris WHERE nif=@nif", con);
                    sql.Parameters.AddWithValue("@nif", nif);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

            //Utilitzarem el mètode DeleteUser per eliminar un usuari de la base de dades
            public static bool DeleteUser(Usuari u)
            {
                bool status = false;
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("DELETE FROM usuaris WHERE nif=@nif", con);
                    sql.Parameters.AddWithValue("@nif", u.Nif);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

            //Utilitzarem el mètode UpdateUser per actualitzar les dades d'un usuari de la base de dades
            public static bool UpdateUser(Usuari u)
            {
                bool status = false;
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("UPDATE usuaris set nom=@nom, cognoms=@cognoms, data_naixement=@data, correu_electronic=@correu, codi_poblacio=@codiPob WHERE nif=@nif", con);
                    sql.Parameters.AddWithValue("@nif", u.Nif);
                    sql.Parameters.AddWithValue("@nom", u.Nom);
                    sql.Parameters.AddWithValue("@cognoms", u.Cognoms);
                    sql.Parameters.AddWithValue("@data", u.Data_naixement);
                    sql.Parameters.AddWithValue("@correu", u.Correu);
                    sql.Parameters.AddWithValue("@codiPob", u.Poblacio);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

        #endregion

        #region Mètodes per treballar amb els events

            //List vs ObservableCollection --> https://www.codeproject.com/Articles/42536/List-vs-ObservableCollection-vs-INotifyPropertyCha
            //Utilitzarem el mètode GetEventsList per obtenir una llista d'events
            //Podem utilitzar la llista per crear controls per cada event.
            public static List<Evento> GetEventsList()
            {
                List<Evento> llista = new List<Evento>();
                Connect();
                MySqlCommand sql = new MySqlCommand("SELECT id, titol, descripcio, data_event, data_publicacio, localitat FROM events ORDER BY data_event, titol ASC", con);
                MySqlDataReader dr = sql.ExecuteReader();
                while (dr.Read())
                {
                    Evento ev = new Evento(
                        dr.GetInt32("id"),
                        dr.GetString("titol"),
                        dr.GetString("descripcio"),
                        dr.GetDateTime("data_event"),
                        dr.GetDateTime("data_publicacio"),
                        dr.GetString("localitat")
                        );
                    llista.Add(ev);
                }
                con.Close();
                return llista;
            }

            //Utilitzarem el mètode GetEventsCollection per obtenir una llista d'events
            //Podem utilitzar la col·lecció per crear controls per cada event.
            public static ObservableCollection<Evento> GetEventsCollection()
            {
                ObservableCollection<Evento> llistaEvents = new ObservableCollection<Evento>();
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("SELECT id, titol, descripcio, data_event, data_publicacio, localitat FROM events ORDER BY data_event, titol ASC", con);
                    MySqlDataReader dr = sql.ExecuteReader();
                    while(dr.Read())
                    {
                        Evento ev = new Evento(
                            dr.GetInt32("id"),
                            dr.GetString("titol"),
                            dr.GetString("descripcio"),
                            dr.GetDateTime("data_event"),
                            dr.GetDateTime("data_publicacio"),
                            dr.GetString("localitat")
                            );
                        llistaEvents.Add(ev);
                    }
                    con.Close();
                }
                catch(Exception e)
                {

                }
                return llistaEvents;
            }

            //Utilitzarem el mètode GetEventCategory per omplir un Combobox amb les categories dels events
            public static DataView GetEventCategory()
            {
                DataTable dt = new DataTable();
                try {
                    Connect();
                    MySqlDataAdapter da = new MySqlDataAdapter("SELECT id, titol, descripcio, data_event, data_publicacio, localitat FROM events ORDER BY data_event, titol ASC", con);
                    da.Fill(dt);
                    con.Close();
                }
                catch(Exception e) { }
                return dt.DefaultView;

            }

            //Utilitzarem el mètode DeleteEvent per eliminar un event de la base de dades
            public static bool DeleteEvent(Int32 id)
            {
                bool status = false;
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("DELETE FROM events WHERE id=@id", con);
                    sql.Parameters.AddWithValue("@id", id);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

        //Utilitzarem el mètode DeleteEvent per eliminar un event de la base de dades
        public static bool DeleteEvent(Evento ev)
            {
                bool status = false;
                try
                {
                    Connect();
                    MySqlCommand sql = new MySqlCommand("DELETE FROM events WHERE id=@id", con);
                    sql.Parameters.AddWithValue("@id", ev.Id);
                    if (sql.ExecuteNonQuery() == 1) status = true;
                    con.Close();
                }
                catch (Exception e)
                {

                }
                return status;
            }

        #endregion

        // Utilitzarem el mètode GetCities per carregar les poblacions dins un ComboBox
        public static DataView GetCities()
        {
            DataTable dt = new DataTable();
            try
            {
                Connect();
                MySqlCommand sql = new MySqlCommand("SELECT codi, nom FROM poblacions ORDER BY nom ASC", con);
                MySqlDataAdapter da = new MySqlDataAdapter(sql);
                da.Fill(dt);

                con.Close();
            }
            catch (Exception e)
            {

            }
            return dt.DefaultView;

        }
    }
}